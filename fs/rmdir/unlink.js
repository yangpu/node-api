/**
 * 用于删除目录的
 * 
 * 删除目录时有一个要求，就是该目录必须为空，所有删除的目录的操作还需要读取目录
 * 和删除文件的操作支持
 */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../files/demo/')

// fs.readdir(pathName, (err, files) => {
//   if (err) {
//     // ENOTEMPTY: directory not empty, rmdir 'e:\learn\node-http\fs\files'
//     console.log(err)
//   }
//   console.log(files)
//   for (let item of files) {
//     let paths = path.join(pathName, item)
//     console.log(paths)
//     fs.unlink(paths, (err) => {
//       if (err) {
//         return console.log(err)
//       }
//       console.log('删除' + item + '成功')
//     }) 
//   }
//   /**
//    * 必须在文件都删完之后才能删文件
//    */
//   fs.rmdir(pathName, (err) => {
//     if (err) {
//       return console.log(err)
//     }
//     console.log('删除目录成功')
//   })
// })


/**
 * demo2
 */
fs.readdir(pathName, async (err, files) => {
  if (err) {
    // ENOTEMPTY: directory not empty, rmdir 'e:\learn\node-http\fs\files'
    console.log(err)
  }

  let promise = files.map(item => {
    return new Promise((resolve, reject) => {
      let paths = path.join(pathName, item)
      console.log(paths)
      fs.unlink(paths, (err) => {
        if (err) {
          console.log(err)
          reject(err)
        }
        console.log('删除' + item + '成功')
        resolve()
      }) 
    })
  })
  
  await Promise.all(promise)

  /**
   * 必须在文件都删完之后才能删文件
   */
  fs.rmdir(pathName, (err) => {
    if (err) {
      return console.log(err)
    }
    console.log('删除目录成功')
  })
})