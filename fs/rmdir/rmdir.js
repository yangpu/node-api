/**
 * 用于删除目录的
 * 
 * 删除目录时有一个要求，就是该目录必须为空，所有删除的目录的操作还需要读取目录
 * 和删除文件的操作支持
 */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../files/mkdirFile')

fs.rmdir(pathName, (err) => {
  if (err) {
    // ENOTEMPTY: directory not empty, rmdir 'e:\learn\node-http\fs\files'
    console.log(err)
  }
})