/**
 * 读取目录
 */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../files/')

console.log('读取files下的文件')

fs.readdir(pathName, (err, files) => {
  if (err) {
    return console.log(err)
  }
  console.log(files)
})