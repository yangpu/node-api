/**
 * nodejs中并没有直接提供文件复制的函数
 * 可以使用写入和读取完成复制的功能
 */
const fs = require('fs')
const path = require('path')

const origin = path.resolve(__dirname, '../files/appendFile.txt')
const target = path.resolve(__dirname, '../files/appendCopyFile.txt')
// 如果没有指定 encoding，则返回原始的 buffer。
// 如果 options 是一个字符串，则指定字符编码：
// fs.readFile('/etc/passwd', 'utf8', callback);

// 读取file
fs.readFile(origin, {
  encoding: 'utf8'
}, (err, data) => {
  if (err) {
    console.log('读取失败')
    return
  }
  // 写入file
  // 如果要修改里面的内容，可以设置data.toString()
  // 或者读取的时候使用utf8

  data = data.replace(/\s/g, '') // 去除空格
  console.log(data) // buffer
  // console.log(data.toString())
  fs.writeFile(target, data, (err) => {
    if (err) {
      console.log('写入文件失败')
      return
    }
    console.log('复制成功')
  })

})