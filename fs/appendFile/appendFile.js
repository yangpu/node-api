/**
 * 文件追加内容
 */

// fs.appenfFile通过异步的方式将文本内容或数据添加到文件里，如果文件不存在会自动创建。

// 添加的数据后分行
// \r回车 \n换行
// window中用 \r\n 
// Linux中用 \n 
// Mac中用 \r
const fs = require('fs')
const path = require('path')
const data = '\r\n追加内容啊'
const pathName = path.resolve(__dirname, '../files/appendFile.txt')

fs.appendFile(pathName, data, (err) => {
  if (err) {
    console.log('追加失败')
  }
  console.log('追加成功')
})
