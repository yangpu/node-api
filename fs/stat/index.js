/**
 * 判断文件的类型
 */
const fs = require('fs')
const path = require('path')

const origin = path.resolve(__dirname, '../files/appendFile.txt')

fs.stat(origin, (err, stats) => {
  // 判断文件是否是文件
  if (err) {
    console.log(err)
    return
  }

  console.log(path.basename(origin) + '是否是文件' + stats.isFile())
})