/**
 * nodejs中并没有直接提供文件复制的函数
 * 可以使用写入和读取完成复制的功能
 */
const fs = require('fs')
const path = require('path')

const target = path.resolve(__dirname, '../files/songCopyFile.txt')


// 读取file
fs.readFile(target, {
  encoding: 'utf8'
}, (err, data) => {
  if (err) {
    console.log('读取失败')
    return
  }
  let lines = data.split('\n')
  // 分，秒， 毫秒
  let reg = /\[(\d{2})\:(\d{2})\.(\d{2})\]\s?(.+)/
  for (let item of lines) {
    let match = reg.exec(item)
    if (match) {
      // console.log(match)
      let minute = match[1]
      let sec = match[2]
      let minsec = match[3]
      
      // 计算歌词的播放时间
      let time = minute * 60 * 1000 + sec * 1000 + parseFloat(minsec)

      // 保存歌词
      let content = match[4]

      // 使用定时器，让每行在指定时间输出
      setTimeout(() => {
        console.log(content)
      }, time)
    }
  }
})