
/**
 * 文件读取
 */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../files/appendFile.txt')

fs.readFile(pathName, (err, data) => {
  if (err) {
    console.log('读取失败')
    return
  }
  // 因为计算机所有的数据都是保存的都是二进制的数据
  // 所以可以调用toString（）方法将二进制数据转换为人类可以识别的字符
  console.log(data)
  console.log(data.toString())
})