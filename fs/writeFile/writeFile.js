const fs = require('fs')
const path = require('path')

const pathName = path.resolve(__dirname, '../files/writeFile.txt')

fs.writeFile(pathName, '异步写入文件', (err) => {
  if (err) {
    console.log('写入失败')
  }
  console.log('写入成功')
})