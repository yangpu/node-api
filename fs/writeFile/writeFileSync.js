/**
 * 同步方式写入文件
 */
const fs = require('fs')
const path = require('path')

// 在进行文件操作的时候，如果是同步的api，必须用try catch来异常
// 不停覆盖文件的，需要使用绝对路径，不能使用相对的，除非是位于顶层
const pathName = path.resolve(__dirname, '../files/writeFile.txt')
// console.log(__dirname)  E:\learn\node-http\fs\writeFile
// console.log(__filename) E:\learn\node-http\fs\writeFile\writeFileSync.js
try {
  console.log('写入文件。。。。')
  fs.writeFileSync(pathName, '我在写文件啊，大哥,你大爷')
} catch (e) {
  console.log(e)
  console.log('不好意思，文件写入失败了')
}