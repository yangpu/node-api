/**
 * 创建目录
 */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../files/mkdirFile')

console.log('在file目录下创建mkdir目录')
// 目录存在会报错的,只能在现有的目录下新建
// 不建议先检测文件是否存在，才新建，直接新建

fs.mkdir(pathName, (err) => {
  if (err) {
    return console.log(err)
  }
  console.log('目录创建成功')
})


// fs.stat(pathName, (err, data) => {
//   if (err) {
//     return console.log(err)
//   }
//   if (data.isDirectory()) {
//     return console.log('目录已经存在了，不用重新创建')
//   } else {
//     fs.mkdir(pathName, (err) => {
//       if (err) {
//         return console.log(err)
//       }
//       console.log('目录创建成功')
//     })
//   }
// })