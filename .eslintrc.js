module.exports = {
  env: {
    browser: true,
    commonjs: true,
    es6: true,
    node: true // 识别node的全局变量
  },
  extends: "eslint:recommended",
  parserOptions: {
    sourceType: "module"
  },
  rules: {
    indent: ["error", 2],
    "linebreak-style": "off",
    "no-console": "off",
    quotes: ["error", "single"],
    semi: ["error", "never"]
  }
};
