//对比缓存 为了更加明显的看到对比缓存，我们将在以下的代码中都将强制缓存关闭
//res.setHeader('Cache-Control','no-cache')

//响应头设置了res.setHeader('Last-Modified',statObj.ctime.toGMTString())
//请求头就会带上req.headers['if-modified-since']

let http = require('http')
let url = require('url')
let util = require('util')
let fs = require('mz/fs')
let stat = util.promisify(fs.stat);
let path = require('path');
let p = path.resolve(__dirname);
http.createServer(async function(req, res) {
    let {pathname} = url.parse(req.url);
    let realPath = path.join(p, pathname);
    console.log(realPath)
    try{
        let statObj = await fs.stat(realPath);
        console.log(statObj)
        // res.setHeader('Cache-Control','max-age=10')  //强制缓存  10s内不需要再次请求服务器
        res.setHeader('Cache-Control','no-cache')
        res.setHeader('Content-Type',require('mime').getType(realPath)+';charset=utf8')
        // res.setHeader('Expires', new Date(Date.now() + 10*1000).toGMTString()) //强制缓存 因为上面设置了no-cache，所以这里的设置其实无效
        let since = req.headers['if-modified-since'];
        if (since === statObj.ctime.toGMTString()) {
            res.statusCode = 304                      //服务器的缓存
            res.end();
        } else {
            res.setHeader('Last-Modified',statObj.ctime.toGMTString())
            fs.createReadStream(realPath).pipe(res)
        }
    }catch(e) {
        res.statusCode = 404;
        res.end('404')
    }
}).listen(3000)
//在浏览器打开localhost:3000/index.html  刷新看到就是304，我们返回状态码304，浏览器就乖乖地去读缓存中的文件了。
//我们稍微改动一下index.html就可以看到 200 