//强制缓存HTTP 1.1 引入的
//Cache-Control: no-cache/max-age=600
let http = require('http')
let url = require('url')
let fs = require('mz/fs')
let path = require('path');
let p = path.resolve(__dirname);
http.createServer(async function(req, res) {

    let {pathname} = url.parse(req.url);
    let realPath = path.join(p, pathname);
    console.log(realPath, '来请求服务了')
    try{
        let statObj = await fs.stat(realPath);
        res.setHeader('Cache-Control','max-age=180')  //强制缓存 10s内不需要再次请求服务器
        //res.setHeader('Cache-Control','no-cache')

        res.setHeader('Content-Type',require('mime').getType(realPath)+';charset=utf8')
       fs.createReadStream(realPath).pipe(res)
    }catch(e) {
        res.statusCode = 404;
        res.end('404')
    }
}).listen(3000)

// 链接：https://juejin.im/post/5b1a7cd16fb9a01e7342d960


// Cache-Control: max-age=180
// Connection: keep-alive
// Content-Type: text/html;charset=utf8
// Date: Sat, 09 Mar 2019 06:17:14 GMT
// Transfer-Encoding: chunked


// html只会重新获取页面，图片缓存了，有catch-Control的字段