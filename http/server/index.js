// https://www.cnblogs.com/chyingp/p/nodejs-learning-express-body-parser.html
const http = require('http')
const util = require('util')

const querystring = require('querystring')

// http.createServer((req, res) => {
//   res.writeHead(200, {'Content-Type': 'text/html'})
//   res.write('<p>hello word</p>')
//   res.end('<p>hello word</p>')
// }).listen(3000)

// console.log('Server is listen to port 3000')






// httpServer事件
/**
 * 1.request: 当客户端请求到来时，该事件被触发，提供两个参数req和res，
  分别是http.ServerRequest 和 http.ServerResponse的实例，表示请求和响应的信息
 * 
 * 
 * 
 * 2.connection: 当TCP连接建立时，该事件会触发，提供一个参数socket，为net.
 * Socket的实例。connection事件的粒度大于request，因为客户端在keey-alive模式下可能会
 * 在同一个连接内发送多次请求
 * 
 * 
 * 
 * 3.close当服务器关闭时，该事件触发，注意不是用户断开连接时
 */

//  最常用的是request,上面例子的显示调用
// const server = new http.Server()
// server.on('request', (req, res) => {
//     res.writeHead(200, {'Content-Type': 'text/html'})
//   res.write('<p>hello word</p>')
//   res.end('<p>hello word</p>')
// })

// server.listen(3000)
// console.log('Server is listen to port 3000')

/**
 * http.ServerRequest
 * http请求的信息，后端最关心的内容，他由httpServer的request事件发送
 * 作为第一个参数req
 *
 * http分为请求头和请求体，请求头解析后立即读取到，
 * 请求头可能较长，需要时间来传输，因此提供以下3个事件来控制请求体的传输
 *
 *
 *
 * 1.data
 * 当数据到来时，该事件触发，该事件提供一个参数chunk，表示接受到的数据
 * 如果事件没监听，请求体将会被抛弃，可能调用多次
 *
 *
 * 2.end
 * 请求体数据传输完毕时触发，此后不会有数据的到来
 *
 * 3.closey
 * 用户请求结束时，触发不同于end，如果用户强制终止的，也是调用close
 */

//  1.获取get内容
// const server = new http.Server();
// server.on("request", (req, res) => {
//   res.writeHead(200, { "Content-Type": "text/plain" });
//   res.end(util.inspect(url.parse(req.url), true));
//   // 通过url.parse，原始的path会解析成一个对象，其中query就是get的内容
//   // 路径就是pathname
// });

// server.listen(3000);
// console.log("Server is listen to port 3000");


// 获取post的内容
/**
 * http.ServerRequest并没有一个属性内容为请求体，原因是
 * 等待请求体传输可能是一件耗时的工作，比如文件上传
nodejs默认不会解析请求体
 */

const server = new http.Server()
server.on('request', (req, res) => {
  let post = ''

  req.on('data', chunk => post += chunk)

  req.on('end', () => {
    // 通过querystring.parse将post解析为真正的post格式，
    // 然后返回给客户端
    // 注意生产中不能这样，会影响效率和安全问题
    post = querystring.parse(post)
    res.end(util.inspect(post))
  })
})

server.listen(3000)
console.log('Server is listen to port 3000')


// http.ServerResponse
/**
 * 3个事件
 * response.WriteHead(statusCode, [headers])
 * 
 * response.write(data, [encoding])默认为utf-8，可以调用多次
 * 
 * response.end(data, [encoding])一次，所以发送已经完毕，如果不调用的画
 * 客户端永远处于pending的状态
 */


