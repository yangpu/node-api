const http = require('http')

let server = new http.Server()

server.on('request', (req,res) => {
  console.log(req)
  res.writeHead('200', {
    'content-type': 'text/plain'
  })
  res.end('nodejs')
})

server.listen(3000)

server.on('close', () => {
  console.log('server close')
})