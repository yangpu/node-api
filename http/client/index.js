/**
 * http模块提供了两个函数
 * 1.http.request
 * 2. http.get
 * 
 * 作为客户端http服务器发起请求
 * 
 * http.request(options, callback)发起http请求
 * options是一个类似关联数组的对象，表示请求的参数，
 * 
 * 1.host：ip或域名
 * 2.port： 端口，默认80
 * 3.method：请求方法，默认是get
 * 4.path：请求的相对于跟地址，默认是/。querystring包含在其中
 * 5.headers: 关联数组，为请求头的内容
 * 
 * 
 * callback是请求的回调函数
 * 传递一个参数为http.ClientResponse的实例
 * http.request返回一个http.ClientRequest的实例
 * 
 * 
 */

const http = require('http')
console.log(111111)
//  const querystring = require('querystring')

//  const contents = querystring.stringify({
//    name: 'node',
//    email: 'node@163.com',
//    address: 'china'
//  })

//  const options = {
//    host: 'www.baidu.com',
//    path: '/',
//    method: 'GET',
//    headers: {
//      'Content-Type': 'application/x-www-form-urlencoded',
//      'Content-Length': contents.length
//    }
//  }

//  const httpClientRequest = http.request(options, res => {
//    res.setEncoding('utf8')
//    res.on('data', (data) => {
//      console.log(data)
//    })
//  })

// // 写入数据到请求主体
//  httpClientRequest.write(contents)
//  // 不要忘了end结束请求，否则服务器不会收到信息
//  httpClientRequest.end()


// 2.更加简单的get方法
/**
  * 自动设置为get请求，同事不需要手动调用req.end()
  */

// http.get({
//   host: 'www.baidu.com'
// }, res => {
//   res.setEncoding('utf8')
//   res.on('data', data => {
//     console.log(data)
//   })
// })



/**
   * http.ClientRequest
   * 由http.get 或者 http.request返回产生对象
   * 表示一个已经产生而且正在进行中的http请求
   * 它提供一个response事件，
   * 也可以显式地绑定这个事件的监听函数
   * 
   * 
   */


const httpClientRequest = http.get({
  host: 'www.baidu.com'
})

httpClientRequest.on('response', res => {
  res.setEncoding('utf8')
  res.on('data', data => {
    console.log(data)
  })
})


/**
* http.ClientRequest和 http.ServerResponse一样也提供write和end
* 函数，用于向服务器发送请求体，通常用于post和put等操作。
* 所有结束必须调用end函数以通知服务器
* 否则请求无效

提供以下的函数
1.request.abort(): 终止发送请求
2.requert.setTimeout(timeout, [callback])：设置请求超时的时间，当请求超时之后，callback调用


http.ClientResponse与http.ServerRequest相似
1.data:数据到达，传递一个参数chunk，表示接收到数据
2.end：传输结束
3.close：连接结束时触发

属性：
1.statusCode:状态码200
2.httpVersion：版本1.0或者1.1
3.headers：http请求头
4.trailers： http请求尾（不常见）

函数
1.response.setEncoding([encoding]):设置编码，当data事件被触发时，数据将以encoding编码，
默认为null，不编码，以buffer形式存储。常见utf8
2.response.pause():暂停接收数据和发送事件，方便实现下载功能
3.response.resume()：从暂停中恢复

*/