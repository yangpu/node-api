// 通过nodejs使用http模块发送请求（详细教程）http://www.php.cn/js-tutorial-403284.html
var http = require('http')

var querystring = require('querystring')

var post_data = {
  a: 123,

  time: new Date().getTime()
} //这是需要提交的数据

var content = querystring.stringify(post_data)

var options = {
  hostname: '127.0.0.1',

  port: 3000,

  path: '/pay/pay_callback',

  method: 'POST',

  headers: {
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
  }
}

var req = http.request(options, function(res) {
  console.log('STATUS: ' + res.statusCode)

  console.log('HEADERS: ' + JSON.stringify(res.headers))

  res.setEncoding('utf8')

  res.on('data', function(chunk) {
    console.log('BODY: ' + chunk)

    //JSON.parse(chunk)
  })
})

req.on('error', function(e) {
  console.log('problem with request: ' + e.message)
})

// write data to request body

req.write(content)

req.end()
