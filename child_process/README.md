## Node 提供了 child_process 模块来创建子进程

- exec - child_process.exec 使用子进程执行命令，缓存子进程的输出，并将子进程的输出以回调函数参数的形式返回。

- spawn - child_process.spawn 使用指定的命令行参数创建新进程。

- fork - child_process.fork 是 spawn()的特殊形式，用于在子进程中运行的模块，如 fork('./son.js') 相当于 spawn('node', ['./son.js']) 。与spawn方法不同的是，fork会在父进程与子进程之间，建立一个通信管道，用于进程之间的通信。


### 区别
- exec() 方法返回最大的缓冲区，**并等待进程结束**，一次性返回缓冲区的内容。
- spawn() 方法返回流 (stdout & stderr)，在进程返回大量数据时使用。**进程一旦开始执行时 spawn() 就开始接收响应**。