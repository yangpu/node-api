/**
 * 拼接缓冲区
 */


 
/**
  * 创建buf
  * 1.传入字节
  * const buf = new Buffer(26)
  * 
  * 2.传入数组
  * const buf = new Buffer([23,23,45])
  * 
  * 3.传入字符串和编码
  * const buf = new Buffer('hello', 'utf8')
  */

// 创建两个缓冲区
const buf1 = new Buffer('世上没难事')
// 通过buffer对象的length属性可以获取buffer缓存中的字节大小
// console.log(buf1.length) // 15？中文栈两个字节？
// console.log(buf1)

const buf2 = new Buffer('只怕有心人')

// 拼接操作
const buf3 = Buffer.concat([buf1, buf2])

console.log(buf3)
console.log(buf3.toString())