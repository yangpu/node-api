// 从缓存区读取数据
// buff.toString(encoding, start, end)

/**
 * 读取缓存区
 */
// 创建一个可以存储26个字节的对象空间
const buf = new Buffer(26)
console.log(buf)

// 向buffer数组中存入26个字母对应得编码
for (let i = 0; i < 26; i++) {
  buf[i] = i + 97
}

// 输出全部字母
console.log(buf.toString('ascii'))

// 输出前5个
console.log(buf.toString('ascii', 0, 5))

// 输出前5个
console.log(buf.toString('utf8', 0, 5))

// 默认utf8输出前5个
console.log(buf.toString(undefined, 0, 5))