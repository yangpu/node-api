/**
 * 写入缓冲区
 */
// 创建一个可以存储5个字节的内存空间对象

const buf = new Buffer(5)

// 通过buffer对象的length属性可以获取buffer缓存中的字节大小
console.log(buf.length)

// 向缓存区写入a
buf.write('a')

// 输出缓存区的数据
console.log(buf)
// [97, 0, 0, 0, 0]


// 向缓存区写入b
// 索引，存入的字节数
buf.write('b', 1, 1, 'ascii')
// [97, 98, 0, 0, 0]

// 输出缓存区的数据
console.log(buf)

console.log(buf.toString())

