/**
 * 由于buffer缓冲区限制在1g
 * 超过1g后没法直接完成读写操作
 * 
 * stream文件刘模块，用来解决大数据的文件操作问题
 * 
 * 
 * http
 * standard input/output
 * 
 * file reads and writes
 * 
 * 
 * Nodejs中，Stream有四种
 * 
 * 1.Readable: 可读操作（可读流）
 * 2.Writable：可写操作（可写流）
 * 3.Duplex： 可读可写操作（双向流，双工流）
 * 4.transform：操作被写入数据，然后读出结果（变换流）
 */

/**
  * 从流中读取数据
  */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../../fs/files/songCopyFile.txt')
let total = ''

//  创建可读流
const readableStream = fs.createReadStream(pathName)

// 设置编码为utf8

readableStream.setEncoding('UTF8')

readableStream.on('data', (chunk) => {
  total += chunk
})

readableStream.on('end', () => {
  console.log(total)
})

readableStream.on('error', (err) => {
  console.log(err.stack)
})


console.log('完毕')