
/**
  * 从流中读取数据
  */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../../fs/files/songCopyFile.txt')
const target = path.resolve(__dirname, '../../fs/files/songFile.txt')
 
//  创建可读流
const readableStream = fs.createReadStream(pathName)

// 创建可写流
const writeableStream = fs.createWriteStream(target)
 
// 设置编码为utf8
 
readableStream.setEncoding('UTF8')
 
readableStream.on('data', (chunk) => {
  // 将读出的模块写入可写流
  writeableStream.write(chunk)
})
 
readableStream.on('end', () => {
  // 将剩下的数据全部写入，并且关闭写入的文件
  console.log('复制完成')
  writeableStream.end()
})
 
readableStream.on('error', (err) => {
  console.log(err.stack)
})
 