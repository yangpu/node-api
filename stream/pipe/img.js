/**
  * 使用pipe进行文件复制
  * node.js使用url下载文件
  * https://blog.csdn.net/rocky0503/article/details/74738030
  * 
  * 
  * 
nodejs下载网络资源的三种方法写入数据:https://blog.csdn.net/davidsu33/article/details/52600906
  */
var http = require('https')  
const fs = require('fs')
const path = require('path')
const target = path.resolve(__dirname, '../../fs/files/meizi2.jpg')
  
// App variables  
// var file_url = 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1546164731397&di=0f1d2b80006e096837a966824fd6f03a&imgtype=0&src=http%3A%2F%2Fwww.it892.com%2Fbaike%2Fuploads%2F201504%2F1428121122orAIX1Jb.jpg'  
var file_url = 'https://i.meizitu.net/2018/12/27c07.jpg'  

http.get(file_url, function(res) { 
  //直接使用Buffer的concat来进行连接，合并Buffer之后一次性写入
  var buffer = new Buffer(0)
  res.on('data', function(data) {  
    console.log(data)
    buffer = Buffer.concat([buffer,data]) 
  }).on('end', function() {  
    console.log('end')
    fs.writeFileSync(target, buffer)
  })  
})