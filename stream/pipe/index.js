/**
  * 使用pipe进行文件复制
  */
const fs = require('fs')
const path = require('path')
const pathName = path.resolve(__dirname, '../../fs/files/down.png')
const target = path.resolve(__dirname, '../../fs/files/songPipeFile.png')


//  创建可读流
const readableStream = fs.createReadStream(pathName)

// 创建可写流
const writeableStream = fs.createWriteStream(target)

// 可以通过可读流的函数pipe接入到可写流中
// pipe（）是一个很高效的数据处理方式

if (readableStream.pipe(writeableStream)) {
  console.log('文件复制成功')
} else {
  console.log('error')
}